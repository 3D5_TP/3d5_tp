package f.esme.a3d5_tp.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import f.esme.a3d5_tp.R

class BasketActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket)
    }
}
