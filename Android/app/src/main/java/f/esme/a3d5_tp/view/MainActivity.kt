package f.esme.a3d5_tp.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import f.esme.a3d5_tp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Declarations des boutons et Initialisation
        val BoutonBasket= findViewById<Button>(R.id.bouton_basket)
        val BoutonRecette = findViewById<Button>(R.id.bouton_recette)
        val BoutonSearch = findViewById<Button>(R.id.bouton_search)

        // Fonction des qu'on click sur bouton ouverture d'une autre activité
        BoutonBasket.setOnClickListener {
            val intent = Intent(this, BasketActivity::class.java);
            startActivity(intent);
            Toast.makeText(this,"Button is clicked", Toast.LENGTH_LONG).show()
        }
        BoutonRecette.setOnClickListener {
            val intent = Intent(this, RecetteActivity::class.java);
            startActivity(intent);
            Toast.makeText(this,"Button is clicked", Toast.LENGTH_LONG).show()
        }

        BoutonSearch.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java);
            startActivity(intent);
            Toast.makeText(this,"Button is clicked", Toast.LENGTH_LONG).show()
        }
    }

}
